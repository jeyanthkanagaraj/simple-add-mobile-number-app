# A Simple Add Mobile Number App 
A Simple add mobile number app to add mobile number and display it with format. It has Drag and Drop functionality to arrange list as per your need.

## Quick Start

```
git clone https://gitlab.com/jeyanthkanagaraj/simple-add-mobile-number-app.git

cd simple-add-mobile-number-app

npm install

npm start
```
