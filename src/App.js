import { useState, useCallback } from "react";
import NumberFormat from "react-number-format";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import update from "immutability-helper";
import { v4 as uuidv4 } from "uuid";
import NumberCard from "./components/NumberCard";
import "./App.css";

function App() {
  const [MobileNumber, setMobileNumber] = useState("");
  const [MobileNumberList, setMobileNumberList] = useState([]);

  //Setting the entered number to the state
  const handleChange = (value) => {
    setMobileNumber(value.formattedValue);
  };

  //Adding the entered number to list on clicking the submit button
  const handleSubmit = (e) => {
    e.preventDefault();
    let list = [
      { id: uuidv4(), mobileNumber: MobileNumber },
      ...MobileNumberList,
    ];
    setMobileNumberList(list);
    setMobileNumber(""); //Resetting the state to default after submitted the form
  };

  //to update the during the drag and drop process
  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = MobileNumberList[dragIndex];
      setMobileNumberList(
        update(MobileNumberList, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
    },
    [MobileNumberList]
  );

  return (
    <div className="App">
      <h3>Please enter Mobile Number below</h3>
      <form onSubmit={handleSubmit}>
        <NumberFormat
          format="(###) ###-####"
          mask=""
          name="phoneNumberInput"
          placeholder="(###) ###-####"
          onValueChange={handleChange}
          value={MobileNumber}
          required
        />
        <button type="submit">Submit</button>
      </form>
      <div className="list-container">
        {MobileNumberList && MobileNumberList.length > 0 ? (
          <DndProvider backend={HTML5Backend}>
            {MobileNumberList.map((number, index) => (
              <NumberCard
                key={number.id}
                index={index}
                id={number.id}
                text={number.mobileNumber}
                moveCard={moveCard}
              />
            ))}
          </DndProvider>
        ) : (
          <p>No mobile number to show</p>
        )}
      </div>
    </div>
  );
}

export default App;
